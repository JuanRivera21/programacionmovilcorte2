package com.example.appmicalculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    private lateinit var txtUsuario:EditText
    private lateinit var txtContra:EditText
    private lateinit var btnIngresar:Button
    private lateinit var btnSalir:Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        eventosClick()


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun iniciarComponentes(){
        txtUsuario=findViewById(R.id.txtUsuario)
        txtContra=findViewById(R.id.txtContraseña)
        btnSalir=findViewById(R.id.btnSalir)
        btnIngresar=findViewById(R.id.btnIngresar)
    }

    public fun eventosClick(){
        btnIngresar.setOnClickListener(View.OnClickListener {
            var usuario : String = getString(R.string.usuario)
            var pass : String = getString(R.string.pass)
            var nombre : String = getString(R.string.name)

            if (txtUsuario.text.toString().contentEquals(usuario) &&
                txtContra.text.toString().contentEquals(pass)){
                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("nombre",nombre)
                startActivity(intent)
            }else{
                Toast.makeText(this, "El usuario y/o contraseña son incorrectos", Toast.LENGTH_LONG).show()
            }
        })

        btnSalir.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

}