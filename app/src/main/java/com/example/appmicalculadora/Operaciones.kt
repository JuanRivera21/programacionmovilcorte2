package com.example.appmicalculadora

class Operaciones(var num1:Float, var num2:Float) {
    //var-variable que cambia valor     val-constante, no puede cambiar de valor
    public fun suma() : Float {
        return this.num1+this.num2
    }

    public fun resta() : Float {
        return this.num1-this.num2
    }

    public fun mult() : Float {
        return this.num1*this.num2
    }

    public fun div() : Float {
        if(this.num1!=0.0f && this.num2!=0.0f)
            return this.num1/num2
        else return 0.0f
    }
}